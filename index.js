#!/usr/bin/env node

const path = require('path')
const program = require('commander')
const configValidator = require('./config-validator')
const colors = require('colors')
const { Document, marks } = require('adf-builder')
const request = require('superagent')
require('superagent-auth-bearer')(request)

program._name = ' ' // hack usage output

program
  .version(require('./package.json').version)
  .usage('mocha test --reporter json | index.js [options]')
  .option('-v, --verbose', 'Enable verbose logging')
  .option('-d, --dry-run', 'Skip posting to Stride if errors occoured')
  .option('-c, --config <config_file>', 'Config for the Stride connection')
  .parse(process.argv)

function sendStrideMessage (doc, config) {
  request
    .post(config.stride.conversationUrl)
    .send(doc.toJSON())
    .authBearer(config.stride.token)
    .end(function (err, res) {
      if (err == null) {
        if (program.verbose >= 1) {
          console.log('Successfully posted to Stride room')
        }
      } else {
        console.error('Failed to notify Stride Room. Error: ', err)
      }
    })
}

function buildStrideMessageDoc (data) {
  const doc = new Document()
  doc.paragraph()
    .text(`Found ${data.failures.length} failed tests`, marks().strong())

  data.failures.forEach(failure => {
    if (program.verbose) {
      console.log(colors.red('✕'), failure.fullTitle)
      console.log(colors.gray(failure.err.message))
    }
    doc.paragraph()
      .text('✕', marks().color('#ff0000').strong())
      .text(' ')
      .text(failure.fullTitle)
      .hardBreak()
      .text(failure.err.message, marks().color('#a0a0a0'))
  })

  return doc
}

function main (params) {
  if (!program.config) {
    console.log()
    console.error('  Error: Missing config')
    program.outputHelp()
    process.exit(1)
  }

  var config = require(path.join(path.resolve(), program.config))
  configValidator.validateConfig(config)

  process.stdin.resume()
  process.stdin.setEncoding('utf8')

  var parsedData = ''

  process.stdin.on('data', function (data) {
    parsedData += data
  })

  process.stdin.on('end', function () {
    var dataJson = JSON.parse(parsedData)
    if (program.verbose) console.log('Found ' + dataJson.failures.length + ' failed tests')

    if (dataJson.failures.length === 0) {
      program.verbose && console.log('No errors found')
      return
    }

    let doc = buildStrideMessageDoc(dataJson)

    console.log(JSON.stringify(doc, null, 2))

    program.verbose && console.log()
    if (!program.dryRun) {
      program.verbose && console.log('Notifying via Stride')
      sendStrideMessage(doc, config)
    } else {
      console.log('Skip posting to Stride due to dry run.')
    }
  })
}

main()
