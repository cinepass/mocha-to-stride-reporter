# mocha-to-stride-reporter
Command-line tool that parses mocha json output and reports failed test to Stride. 


## Install

`[sudo] npm install -g mocha-to-stride-reporter`

## Usage

All it needs is running the mocha tests with the `json` reporter and pipe it into `mocha-to-stride-reporter`, 
which requires a config for connecting to stride.

`mocha --reporter json | mocha-to-stride-reporter -c config.json`

## Setup 

You need to create an access token for the room you want to post the test failures into. 

1. Open the target room in Stride 
2. Open the Apps panel & click the Plus button
3. Click "Add custom app"
4. Select "API tokens" and generate new token. 
5. Copy the access token & conversation into the config 

### Config template

```json
{
  "stride": {
    "token": "",
    "conversationUrl": ""
  }
}
```
